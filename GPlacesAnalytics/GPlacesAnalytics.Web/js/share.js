    Share = {
        vkontakte: function(purl, ptitle, pimg, text) {
            url  = 'http://vkontakte.ru/share.php?';
            url += 'url='          + encodeURIComponent(purl);
            url += '&title='       + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image='       + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);
        },
        odnoklassniki: function(purl, text) {
            url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(text);
            url += '&st._surl='    + encodeURIComponent(purl);
            Share.popup(url);
        },

        facebook: function(purl, ptitle, pimg, text) {
            url  = 'https://www.facebook.com/dialog/feed?';
            url += 'app_id='          + encodeURIComponent("539190726207460");
            url += '&link='           +encodeURIComponent(purl);
            url += '&picture='       + encodeURIComponent(pimg);
            url += '&name='          + encodeURIComponent(ptitle);
            url += '&description='   + encodeURIComponent(text);
            url += '&redirect_uri='  + encodeURIComponent(purl);
            url += '&display=popup';
            Share.popup(url);
        },

        twitter: function(purl, ptitle) {
            url  = 'http://twitter.com/share?';
            url += 'text='      + encodeURIComponent(ptitle);
            url += '&url='      + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        mailru: function(purl, ptitle, pimg, text) {
            url  = 'http://connect.mail.ru/share?';
            url += 'url='          + encodeURIComponent(purl);
            url += '&title='       + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl='    + encodeURIComponent(pimg);
            Share.popup(url)
        },
        google: function(purl) {
            url = 'https://plus.google.com/share?';
            url += 'url='          + encodeURIComponent(purl);
            Share.popup(url)
        },

        popup: function(url) {
            console.log(url);
            window.open(url,'','toolbar=0,status=0,width=626,height=436');
        }
    };

