﻿$(document).ready(function () {
    //при загрузке документа
    //console.log("jquery dom ready");
    var URL = "http://www.georate.ru/ ";
    var image = 'images/ico3.svg';
    var description = "";
    var title = "";

    $("#s_analyze").hide();
    $("#sidebar").hide();
    $("img#loader").attr('src', 'images/477.gif');
    $("#navigation").css('width', '100%');

    $("head meta[property='og:image']").attr("content", image);

    $(".load_button").on("click", function (event) {
        $('#preloader').fadeOut("slow");
        //$('map').fadeIn();
        deepSearchRequest();
    });

    $(".analyze_button input[type='button']").on("click", function (event) {
        deepSearchRequest();
    });

    $("div.social_sharing a#vk").on('click', function (event) {
        Share.vkontakte(URL, title, image, description)
    });
    $("div.social_sharing a#fb").on('click', function (event) {
        Share.facebook(URL, title, image, description)
    });
    $("div.social_sharing a#twitter").on('click', function (event) {
        var title1 = title + " " + description + ":";
        Share.twitter(URL, title1)
    });
    $("div.social_sharing a#mail").on('click', function (event) {
        Share.mailru(URL, title, image, description)
    });
    $("div.social_sharing a#google").on('click', function (event) {
        Share.google(URL)
    });

    var rList = {};

// Просим фрейм подгрузить все страницы результатов из google
    function deepSearchRequest() {
        var frame = document.getElementById("map_iframe");                 // send the message
        if (frame.contentWindow.postMessage) {
            frame.contentWindow.postMessage(JSON.stringify({ "messageType": "deepSearchRequest" }), "*");
        }
        else {
            alert("Your browser does not support the postMessage method!");
        }
    }


// Разбираем, что фрейм прислал в ответ
    function OnMessage(evt) {
        var evtData = JSON.parse(evt.data);
        if (evtData["messageType"] == "deepSearchResults") {
            rList = evtData["resultListDeep"];
            var rReport = evtData["resultReport"];
            var tmpStr = "";
            if (rList.length > 0) {
                for (var i = 0; i < rList.length; i++) {
                    tmpStr = tmpStr + "<p id=\"" + i + "\" onclick=\"placeInfoRequest(this.id)\" >" + rList[i].name + "</p>";
                    if (i == 20) {
                        break;
                    }
                    ; // выводим в сайдбар только 20 рез-тов
                }
                ;
            }
            ;
            document.getElementById("found_objects").innerHTML = tmpStr;
            document.getElementById("s_percent").innerHTML = rReport[0];
            document.getElementById("s_comment").innerHTML = rReport[1];

            //задаем значения для шаринга и навешиваем ивенты на кнопки
            title = "Рейтинг " + $("p#s_percent").text() + " " + $("p#s_comment").text();
            $("head meta[property='og:title']").attr("content", title);
            $("head meta[property='twitter:title']").attr("content", title);
            description = "Мое местоположение было оценено с помощью приложения georate";
            $("head meta[property='og:description']").attr("content", description);
            $("#navigation").css('width', '75%');
            $("#sidebar").show();
        } else {
            if (evtData["messageType"] == "mapReady") {
                // Карта готова
                $("img#loader").hide();
                $("#s_analyze").show();
            }
            ;
        }
        ;

    }

    (window.addEventListener && window.addEventListener("message", OnMessage, false)    // FF,SA,CH,OP,IE9
        || window.attachEvent && window.attachEvent("onmessage", OnMessage));                // IE8

    window.onload = function () {
        var newHeight = $(window).height() - $("#rreport").outerHeight(true) - $("#ssharing").outerHeight(true) - 80; // откуда 80?
        $("#rstats").css("height", newHeight);
    };
});

function placeInfoRequest(markerIndex) {
    var frame = document.getElementById("map_iframe");
    if (frame.contentWindow.postMessage) {
        frame.contentWindow.postMessage(JSON.stringify({ "messageType": "showPlaceInfo", "markerIndex": markerIndex }), "*");
    }
    else {
        alert("Your browser does not support the postMessage method!");
    }
};