$(document).ready(function () {
    console.log("jquery dom ready");

    var URL = "http://www.geora.te/ ";
    var title = "Рейтинг " + $("p#s_percent").text() + " " + $("p#s_comment").text();
    $('head').append(toStaticHTML("<meta property='og:title' content='" + title + "'/>"));

    var description = "Мое местоположение было оценено с помощью приложения georate";
    $('head').append(toStaticHTML("<meta property='og:description' content='" + description + "'/>"));

    var image = 'https://pbs.twimg.com/profile_images/1959307617/marmota.jpg';
    $('head').append(toStaticHTML("<meta property='og:image' content='" + image + "'/>"));

    var buttons = $(".social_sharing").find("a");
    $("div.social_sharing a#vk").on('click', function (event) {
        Share.vkontakte(URL, title, image, description)
    });
    $("div.social_sharing a#fb").on('click', function (event) {
        Share.facebook(URL, title, image, description)
    });
    $("div.social_sharing a#twitter").on('click', function (event) {
        var title1 = title + " " + description + ":";
        Share.twitter(URL, title1)
    });
    $("div.social_sharing a#mail").on('click', function (event) {
        Share.mailru(URL, title, image, description)
    });
    $("div.social_sharing a#google").on('click', function (event) {
        $('head').append(toStaticHTML("<meta name='description' content=" + description + " />"));
        Share.google(URL)
    });

    Share = {
        vkontakte: function (purl, ptitle, pimg, text) {
            url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);
        },
        odnoklassniki: function (purl, text) {
            url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(text);
            url += '&st._surl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        /*facebook: function(purl, ptitle, pimg, text) {
            url  = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]='     + encodeURIComponent(ptitle);
            url += '&p[summary]='   + encodeURIComponent(text);
            url += '&p[url]='       + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);
        },*/
        facebook: function (purl, ptitle, pimg, text) {
            url = 'https://www.facebook.com/dialog/feed?';
            url += 'app_id=' + encodeURIComponent("539190726207460");
            url += '&link=' + encodeURIComponent(purl);
            url += '&picture=' + encodeURIComponent(pimg);
            url += '&name=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&redirect_uri=' + encodeURIComponent(purl);
            url += '&display=popup';
            Share.popup(url);
        },

        twitter: function (purl, ptitle) {
            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        mailru: function (purl, ptitle, pimg, text) {
            url = 'http://connect.mail.ru/share?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl=' + encodeURIComponent(pimg);
            Share.popup(url)
        },
        google: function (purl) {
            url = 'https://plus.google.com/share?';
            url += 'url=' + encodeURIComponent(purl);
            Share.popup(url)
        },

        popup: function (url) {
            console.log(url);
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }
    };
});
