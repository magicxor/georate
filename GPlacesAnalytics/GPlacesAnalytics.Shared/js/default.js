﻿// Основные сведения о пустом шаблоне см. в следующей документации:
// http://go.microsoft.com/fwlink/?LinkID=392286
(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: Это приложение было вновь запущено. Инициализируйте
                // приложение здесь.
            } else {
                // TODO: Это приложение вновь активировано после приостановки.
                // Восстановите состояние приложения здесь.
            }
            args.setPromise(WinJS.UI.processAll());
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: Это приложение будет приостановлено. Сохраните здесь все состояния,
        // которые необходимо сохранять во время приостановки. Можно использовать
        // объект WinJS.Application.sessionState, который автоматически
        // сохраняется и восстанавливается при приостановке. Если перед приостановкой приложения необходимо
        // выполнить асинхронную операцию, вызовите метод
        // args.setPromise().
    };

    app.start();
})();

var rList = {};

// Просим фрейм подгрузить все страницы результатов из google
function deepSearchRequest() {
    var frame = document.getElementById("map_iframe");                 // send the message 
    if (frame.contentWindow.postMessage) {
        frame.contentWindow.postMessage(JSON.stringify({ "messageType": "deepSearchRequest" }), "*");
    }
    else { alert("Your browser does not support the postMessage method!"); }
}

function placeInfoRequest(markerIndex) {
    var frame = document.getElementById("map_iframe");
    if (frame.contentWindow.postMessage) {
        frame.contentWindow.postMessage(JSON.stringify({ "messageType": "showPlaceInfo", "markerIndex": markerIndex }), "*");
    }
    else { alert("Your browser does not support the postMessage method!"); }
};

// Разбираем, что фрейм прислал в ответ
function OnMessage(evt) {
    var evtData = JSON.parse(evt.data);
    if (evtData["messageType"] == "deepSearchResults") {
        rList = evtData["resultListDeep"];
        var rReport = evtData["resultReport"];
        var tmpStr = "";
        if (rList.length > 0) {
            for (var i = 0; i < rList.length; i++) {
                tmpStr = tmpStr + "<p id=\"" + i + "\" onclick=\"placeInfoRequest(this.id)\" >" + rList[i].name + "</p>";
                if (i == 20) { break; }; // выводим в сайдбар только 20 рез-тов
            };
        };
        MSApp.execUnsafeLocalFunction(function () {
            document.getElementById("found_objects").innerHTML = tmpStr;
            document.getElementById("s_percent").innerHTML = rReport[0];
            document.getElementById("s_comment").innerHTML = rReport[1];
        });
    } else {
        if (evtData["messageType"] == "mapReady") {
            // Карта готова
        };
    };

}

(window.addEventListener && window.addEventListener("message", OnMessage, false)    // FF,SA,CH,OP,IE9 
 || window.attachEvent && window.attachEvent("onmessage", OnMessage));                // IE8 

window.onload = function () {
    var newHeight = $(window).height() - $("#rreport").outerHeight(true) - $("#ssharing").outerHeight(true) - 80; // откуда 80? 
    $("#rstats").css("height", newHeight);
}