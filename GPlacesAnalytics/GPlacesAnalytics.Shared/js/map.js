﻿var pos;
var map;
var defaultPos = new google.maps.LatLng(52.28626, 104.280488); //Сквер Кирова; Координаты Москвы 55.763585, 37.560883
var markers = [];
var smarkers = [];
var resultListDeep;
var evsrc;
var nextPageFlag = false;
var hostnameRegexp = new RegExp('^https?://.+?/');
var places, iw;

// тип: [желаемое_число_объектов, вес_рубрики_от_0_до_1]
var placeTypesJSON = {
    "bus_station": [2, 1],
    // "subway_station": [1, 1],
    // "train_station": [1, 0.5],
    //
    "department_store": [1, 0.3],
    //"convenience_store": [1, 0.3],
    //"store": [1, 0.5],
    //
    "hospital": [1, 0.8],
    //"health": [2, 0.8],
    "pharmacy": [2, 1],
    //
    "school": [1, 1],
    "post_office": [1, 0.7],
    "park": [1, 0.6],
    "gym": [1, 0.8]
};
var placeTypesArr = [];

function initialize() {
    jQuery.each(placeTypesJSON, function (key, value) {
        placeTypesArr.push(key);
    });

    pos = defaultPos;

    map = new google.maps.Map(document.getElementById('mapdisplay'), {
        zoom: 17,
        center: pos,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        panControl: true, // джойстик
        zoomControl: true,
    /*    zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.LEFT_CENTER
        },*/
        mapTypeControl: true,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false
    });
    places = new google.maps.places.PlacesService(map);

    // SearchBox
    // Create the search box and link it to the UI element.
    var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var searchBox = new google.maps.places.SearchBox(
      /** @type {HTMLInputElement} */(input));

    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function() {
        var splaces = searchBox.getPlaces();

        if (splaces.length == 0) {
            return;
        }
        for (var i = 0, smarker; smarker = smarkers[i]; i++) {
            smarker.setMap(null);
        }

        // For each place, get the icon, place name, and location.
        smarkers = [];
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = splaces[i]; i++) {
            var image = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a smarker for each place.
            var smarker = new google.maps.Marker({
                map: map,
                icon: image,
                title: place.name,
                position: place.geometry.location
            });

            smarkers.push(smarker);

            bounds.extend(place.geometry.location);
        }

        map.fitBounds(bounds);
    });

    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(map, 'bounds_changed', function() {
        var bounds = map.getBounds();
        searchBox.setBounds(bounds);
    });
    // End of SearchBox

    // Try HTML5 geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            pos = new google.maps.LatLng(position.coords.latitude,
                position.coords.longitude);

            var infowindow = new google.maps.InfoWindow({
                map: map,
                position: pos,
                content: 'Вы здесь'
            });

            map.setCenter(pos);
        }, function () {
            handleNoGeolocation(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
    // End of HTML5 geolocation

    var resultFull = { "messageType": "mapReady" };
    top.postMessage(JSON.stringify(resultFull), "*");
}

function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
    } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var options = {
        map: map,
        position: pos,
        content: content
    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
}

function deepSearchCBPlaces() {
    var request = {
        location: map.getCenter(),
        radius: 500,
        types: placeTypesArr
    };
    infowindow = new google.maps.InfoWindow();
    resultListDeep = [];
    nextPageFlag = false;
    //var service = new google.maps.places.PlacesService(map);
    //service.nearbySearch(request, callbackDeep);
    places.nearbySearch(request, callbackDeep);
}

function callbackDeep(results, status, pagination) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            resultListDeep.push(results[i]);
            if (i <= 20 && nextPageFlag == false) { // отображаем на карте только 20 булавок, остальные скрытые
                createMarker(results[i], resultListDeep.length - 1, true);
            } else {
                createMarker(results[i], resultListDeep.length - 1, false);
            };
        };
        if (pagination.hasNextPage) {
            nextPageFlag = true;
            setTimeout(pagination.nextPage(), 2000);
        } else {
            sendResults(false);
        };
    } else {
        sendResults(true);
    };
}

function sendResults(isZero) {
    var ratingPercent;
    if (isZero == true) {
        ratingPercent = 0;
    } else {
        ratingPercent = calcRating();
    };
    var ratingComment = getRatingComment(ratingPercent);
    var resultReport = [ratingPercent + "%", ratingComment];
    var resultFull = { "messageType": "deepSearchResults", "resultListDeep": resultListDeep, "resultReport": resultReport };
    evsrc.postMessage(JSON.stringify(resultFull), "*");
}

function calcRating() {
    // рассчитываем рейтинг места для отображения в панели
    var maxRating = 0; // максимально возможный общий абсолютный рейтинг
    var curRating = 0; // общий абсолютный рейтинг
    jQuery.each(placeTypesJSON, function (key, value) { // последовательно вычисляем рейтинг всех категорий объектов для нашего местоположения
        maxRating = maxRating + value[1];               // заодно рассчитаем максимально возможный общий абсолютный рейтинг
        var placeWeight = value[1] / value[0];          // сколько привносит 1 объект в данную рубрику = вес_рубрики/желаемое число объектов
        var curTypeRating = 0;
        for (var i = 0; i < resultListDeep.length; i++) {              // ищем среди всех объектов объекты из данной рубрики            
            for (var j = 0; j < resultListDeep[i].types.length; j++) { // у одного объекта может быть много типов, просматриваем все
                if (resultListDeep[i].types[j] == key) {               // если тип объекта совпал с искомой рубрикой
                    if (curTypeRating < value[1]) {                    // и если максимальный вес этой рубрики ещё не достигнут
                        curTypeRating = curTypeRating + placeWeight;   // то увеличиваем абсолютный рейтинг рубрики
                    };
                };
            };
        };
        curRating = curRating + curTypeRating;
    });
    var curPercentRating = (curRating * 100) / maxRating;
    return Math.round(curPercentRating);
}

function getRatingComment(ratingPercent) {
    var rKey = "0";
    var ratingComments = {
        "0": [
        "Ископаемых нет. Воды нет. Растительности нет. Населена роботами.",
        "Уютный домик уральского отшельника не сразу заметишь.",
        "Жизнь была прекрасна",
        "Передай дьяволу, что это я прислал тебя к нему!",
        "Я так давно миновал точку невозвращения, что забыл, как она выглядит.",
        "Я бы засмеялся, если бы помнил как это делается.",
        "В жизни все случается не так, как ожидаешь."],
        "31": [
            "Дом там, где тебе хорошо.",
            "Если единственный выбор является неправильным, то это скорее не выбор, а судьба.",
            "Я искал это место, чтобы получить ответы. А когда нашёл, все вопросы были забыты.",
            "Повернуться и уйти, скрыться из города… Это было бы самым мудрым решением. Я был не настолько мудр.",
            "В стране слепых и кривой – король.",
            "Хэппи-энда не будет.",
            "Скоро рассвет. Прощай, американская мечта.",
            "Все субъективно – правильное, неправильное, добро и зло."
        ],
        "51": [
            "Я вошёл, оставив снаружи тьму и холод. Город снаружи бушевал, как разъярённый зверь.",
            "Мои желания уже сделали выбор за меня.",
            "Правильно или неправильно — это лишь иллюзия."
        ],
        "71": [
            "Если бы только мечты не забывались, когда ты их добился!"
        ],
        "91": [
            "Настоящий воздушный замок, ставший реальностью.",
            "Где-то подо мной пульсировало чудовищное сердце мегаполиса."
        ]
    }

    if (ratingPercent <= 30) {
        rKey = "0";
    };
    if (ratingPercent >= 31 && ratingPercent <= 50) {
        rKey = "31";
    };
    if (ratingPercent >= 51 && ratingPercent <= 70) {
        rKey = "51";
    };
    if (ratingPercent >= 71 && ratingPercent <= 90) {
        rKey = "71";
    };
    if (ratingPercent >= 91 && ratingPercent <= 100) {
        rKey = "91";
    };

    var randInd = 0;
    randInd = Math.round(Math.random() * (ratingComments[rKey].length - 1));

    return ratingComments[rKey][randInd];
}

function createMarker(place, ind, visibleFlag) {
    //var placeLoc = place.geometry.location; // из примера, не удалять
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location,
        animation: google.maps.Animation.DROP,
        visible: visibleFlag
    });

    google.maps.event.addListener(marker, 'click', getDetails(resultListDeep[ind], ind));
        /*function () {
        infowindow.setContent(place.name);
        infowindow.open(map, this);

    });*/
    window.setTimeout(dropMarker(ind), ind * 200);
    markers.push(marker);
}

function dropMarker(i) {
    return function () {
        if (markers[i]) {
            markers[i].setMap(map);
        }
    }
}

// Sets the map on all markers in the array.
function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setAllMap(null);
}

////////////////////////////// подробная инфа о месте по клику в сайдбаре
function getDetails(result, i) {
    return function () {
        places.getDetails({
            reference: result.reference
        }, showInfoWindow(i));
    }
}

function showInfoWindow(i) {
    return function (place, status) {
        if (iw) {
            iw.close();
            iw = null;
        }

        if (status == google.maps.places.PlacesServiceStatus.OK) {
            iw = new google.maps.InfoWindow({
                content: getIWContent(place)
            });
            iw.open(map, markers[i]);
        }
    }
}

function getIWContent(place) {
    var content = '';
    content += '<table>';
    content += '<tr class="iw_table_row">';
    content += '<td style="text-align: right"><img class="hotelIcon" src="' + place.icon + '"/></td>';
    content += '<td><b><a href="' + place.url + '">' + place.name + '</a></b></td></tr>';
    content += '<tr class="iw_table_row"><td class="iw_attribute_name">Address:</td><td>' + place.vicinity + '</td></tr>';
    if (place.formatted_phone_number) {
        content += '<tr class="iw_table_row"><td class="iw_attribute_name">Telephone:</td><td>' + place.formatted_phone_number + '</td></tr>';
    }
    if (place.rating) {
        var ratingHtml = '';
        for (var i = 0; i < 5; i++) {
            if (place.rating < (i + 0.5)) {
                ratingHtml += '&#10025;';
            } else {
                ratingHtml += '&#10029;';
            }
        }
        content += '<tr class="iw_table_row"><td class="iw_attribute_name">Rating:</td><td><span id="rating">' + ratingHtml + '</span></td></tr>';
    }
    if (place.website) {
        var fullUrl = place.website;
        var website = hostnameRegexp.exec(place.website);
        if (website == null) {
            website = 'http://' + place.website + '/';
            fullUrl = website;
        }
        content += '<tr class="iw_table_row"><td class="iw_attribute_name">Website:</td><td><a href="' + fullUrl + '">' + website + '</a></td></tr>';
    }
    content += '</table>';
    return content;
}
////////////////////////////////////////////


// Инициализация при загрузке фрейма
google.maps.event.addDomListener(window, 'load', initialize);

function OnMessage(evt) {
    var evtData = JSON.parse(evt.data);
    if (evtData["messageType"] == "deepSearchRequest") {
        evsrc = evt.source;
        clearMarkers();
        markers = [];
        deepSearchCBPlaces();
    } else {
        if (evtData["messageType"] == "showPlaceInfo") {
            google.maps.event.trigger(markers[evtData["markerIndex"]], 'click');
        };
    };
}

(window.addEventListener && window.addEventListener('message', OnMessage, false)  // FF,SA,CH,OP,IE9+
 || window.attachEvent && window.attachEvent('onmessage', OnMessage));              // IE8